# WeatherApp

This project is built with Flutter version 3.10.6.

## Prerequisites

Before running this project, make sure you have the following installed on your machine:

- Flutter SDK: Make sure you have Flutter version 3.10.6 installed. You can check the installed version by running `flutter --version` in your terminal. If you have a different version of Flutter installed, you can switch to version 3.10.6 by running `flutter versions` to see the available versions and then `flutter version <version_number>` to switch to the desired version.

## Getting Started

1. Clone this repository to your local machine:
https://gitlab.com/binhdang99/weatherapp.git

2. Change to the project directory: cd WeatherApp

3. Install the project dependencies: flutter pub get

This command downloads the dependencies specified in the `pubspec.yaml` file. Make sure you have Flutter version 3.10.6 installed, as mentioned in the prerequisites section, to ensure compatibility with the dependencies used in this project.

4. Run the project: flutter run

This command launches the app on the connected device or emulator.

## Building the App

To build the app in release mode, follow these steps:

### Android

1. Open a terminal or command prompt.

2. Navigate to the project directory: cd WeatherApp

3. Run the following command to build the Android APK: flutter build apk --release

If an error occurs with the icon, use the command: flutter build apk --release --no-tree-shake-icons

This command compiles the Flutter app into an APK file in release mode, while disabling tree shaking for icons. The generated APK file can be found in the `build/app/outputs/flutter-apk` directory. Using the `--no-tree-shake-icons` option can help resolve issues related to missing or incorrectly displayed icons.

### IOS
iOS development for this project is currently in progress. Stay tuned for updates on iOS-specific build instructions

## Project Structure

- `lib/`: Contains the Dart source code files for the application.
- `pubspec.yaml`: Defines the project dependencies and configuration.

Feel free to modify the source code files in the `lib/` directory to customize the app according to your needs. You can use any code editor or IDE that supports Flutter development.