import '../strings_enum.dart';

final Map<String, String> viVn = {
  Strings.appName : 'Ứng dụng thời tiết',
  Strings.hello: 'Xin chào!',
  Strings.loading: 'Đang tải...',
  Strings.changeTheme: 'Thay đổi giao diện',
  Strings.changeLanguage: 'Thay đổi ngôn ngữ',
  Strings.noInternetConnection: 'Không có kết nối internet!',
  Strings.serverNotResponding: 'Máy chủ không phản hồi!',
  Strings.someThingWentWorng: 'Có lỗi xảy ra',
  Strings.apiNotFound: 'Không tìm thấy đường dẫn!',
  Strings.serverError: 'Lỗi máy chủ',
  Strings.urlNotFound: 'Không tìm thấy URL',
  Strings.retry: 'Thử lại',
  Strings.welcomScreenTitle:
      'Khám phá bản đồ toàn cầu về gió, thời tiết và điều kiện đại dương',
  Strings.welcomScreenSubtitle:
      'Lập kế hoạch cho chuyến đi của bạn trở nên dễ dàng hơn với ứng dụng thời tiết Ideate. Bạn có thể xem ngay lập tức thông tin thời tiết của toàn cầu chỉ trong vài giây',
  Strings.getStarted: 'Bắt đầu',
  Strings.alreadyHaveAnAccount: 'Đã có tài khoản? ',
  Strings.login: 'Đăng nhập',
  Strings.helloUser: 'Xin chào Binh',
  Strings.discoverTheWeather: 'Khám phá thời tiết',
  Strings.aroundTheWorld: 'Trên toàn thế giới',
  Strings.weatherNow: 'Thời tiết hiện tại',
  Strings.wind: 'Gió',
  Strings.pressure: 'Áp suất',
  Strings.locationPermissionNeeded: 'Yêu cầu quyền vị trí',
  Strings.pleaseEnableLocationPermission:
      'Vui lòng cho phép quyền vị trí để có thông tin thời tiết chính xác hơn',
  Strings.allowLocation: 'Cho phép vị trí',
  Strings.hoursForecast: 'Dự báo 24 giờ',
  Strings.sunrise: 'Bình minh',
  Strings.sunset: 'Hoàng hôn',
  Strings.humidity: 'Độ ẩm',
  Strings.realFeel: 'Cảm giác thực',
  Strings.uv: 'Tia tử ngoại',
  Strings.chanceOfRain: 'Cơ hội có mưa',
  Strings.today: 'Hôm nay',
  Strings.now: 'hiện tại',
  Strings.kmh: 'km/h',
  Strings.celsius: '°',
  Strings.north: 'Bắc',
  Strings.south: 'Nam',
  Strings.east: 'Đông',
  Strings.west: 'Tây',
  Strings.northeast: 'Đông Bắc',
  Strings.southeast: 'Đông Nam',
  Strings.southwest: 'Tây Nam',
  Strings.northwest: 'Tây Bắc',
  Strings.searchCity : 'Tìm kiếm thành phố',
  Strings.confirmChangeLanguage : 'Xác nhận',
  Strings.cancelChangeLanguage : 'Hủy bỏ',
};
