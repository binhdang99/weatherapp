import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/config/translations/vi_VN/vi_vn_translation.dart';

import '../../app/data/local/shared_pref.dart';
import 'en_US/en_us_translation.dart';

class LocalizationService extends Translations {
  LocalizationService._();

  static LocalizationService? _instance;

  static LocalizationService getInstance() {
    _instance ??= LocalizationService._();
    return _instance!;
  }

  static Locale defaultLanguage = supportedLanguages['en']!;

  static Map<String,Locale> supportedLanguages = {
    'en' : const Locale('en', 'US'),
    'vi' : const Locale('vi', 'VN'),
  };

  static Map<String,TextStyle> supportedLanguagesFontsFamilies = {
    'en' : const TextStyle(fontFamily: 'Roboto'),
    'vi' : const TextStyle(fontFamily: 'Roboto'),
  };

  @override
  Map<String, Map<String, String>> get keys => {
    'en_US': enUs,
    'vi_VN': viVn,
  };

  static isLanguageSupported(String languageCode) =>
    supportedLanguages.keys.contains(languageCode);


  static updateLanguage(String languageCode) async {
    if(!isLanguageSupported(languageCode)) return;
    await SharedPref.setCurrentLanguage(languageCode);
    if(!Get.testMode) {
      Get.updateLocale(supportedLanguages[languageCode]!);
    }
  }

  static bool isItEnglish() =>
      SharedPref.getCurrentLocal().languageCode.toLowerCase().contains('en');

  static Locale getCurrentLocal () => SharedPref.getCurrentLocal();
}
