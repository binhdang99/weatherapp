import 'package:get/get.dart';
import 'package:weather_app/app/bindings/app_binding.dart';

import '../views/home_view/home_view.dart';
import '../views/splash_view/splash_view.dart';
import '../views/weather_view/weather_view.dart';
import '../views/welcome_view/welcome_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();
  

  static const initial = Routes.splash;

  static final routes = [
    GetPage(
      name: _Paths.splash,
      page: () => const SplashView(),
      binding: AppBindings(),
    ),
    GetPage(
      name: _Paths.welcome,
      page: () => const WelcomeView(),
      binding: AppBindings(),
    ),
    GetPage(
      name: _Paths.home,
      page: () => const HomeView(),
      binding: AppBindings(),
    ),
    GetPage(
      name: _Paths.weather,
      page: () => const WeatherView(),
      binding: AppBindings(),
      transition: Transition.rightToLeft,
      transitionDuration: const Duration(milliseconds: 250),
    ),
  ];

}
