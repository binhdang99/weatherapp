import 'package:get/get.dart';
import 'package:weather_app/app/controllers/home_controller.dart';
import 'package:weather_app/app/controllers/splash_controller.dart';
import 'package:weather_app/app/controllers/weather_controller.dart';

import '../controllers/welcome_controller.dart';

class AppBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(SplashController());
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<WeatherController>(
      () => WeatherController(),
    );
    Get.lazyPut<WelcomeController>(
      () => WelcomeController(),
    );
  }
}
