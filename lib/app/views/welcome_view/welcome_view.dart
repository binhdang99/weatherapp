import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../config/translations/strings_enum.dart';
import '../../widgets/custom_button.dart';
import '../../controllers/welcome_controller.dart';

class WelcomeView extends GetView<WelcomeController> {
  const WelcomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final theme = context.theme;
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Container(
              color: context.theme.primaryColor,
            ),
          ),
          Positioned(
            bottom: 20.h,
            left: 20.w,
            right: 20.w,
            child: Container(
              height: 360.h,
              padding: EdgeInsets.fromLTRB(20.w, 12.h, 20.w, 27.h),
              decoration: BoxDecoration(
                color: theme.cardColor,
                borderRadius: BorderRadius.circular(30.r),
              ),
              child: Column(
                children: [
                  24.verticalSpace,
                  Text(
                    Strings.welcomScreenTitle.tr,
                    style: theme.textTheme.displayMedium,
                    textAlign: TextAlign.center,
                  ),
                  16.verticalSpace,
                  Text(
                    Strings.welcomScreenSubtitle.tr,
                    style: theme.textTheme.bodyMedium?.copyWith(height: 1),
                    textAlign: TextAlign.center,
                  ),
                  const Spacer(),
                  GetBuilder<WelcomeController>(
                    builder: (context) {
                      return CustomButton(
                        onPressed: () async => await controller.routeHome(),
                        text: Strings.getStarted.tr,
                        fontSize: 18.sp,
                        backgroundColor: theme.primaryColor,
                        foregroundColor: Colors.white,
                        width: 265.w,
                        radius: 30.r,
                        verticalPadding: 20.h,
                      );
                    }
                  ),
                  20.verticalSpace,
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
