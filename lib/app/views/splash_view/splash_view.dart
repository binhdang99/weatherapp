import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/config/theme/light_theme_colors.dart';

import '../../../utils/constants.dart';
import '../../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  const SplashView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: LightThemeColors.bgSplash,
      body: Center(
        child: Image.asset(Constants.weatherAnimation, fit: BoxFit.fitWidth,),
      ),
    );
  }
}