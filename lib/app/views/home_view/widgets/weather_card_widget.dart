import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:weather_app/utils/constants.dart';

import '../../../../utils/extensions.dart';
import '../../../../config/translations/strings_enum.dart';
import '../../../widgets/custom_cached_image.dart';
import '../../../data/models/weather_model.dart';
import '../../../routes/app_pages.dart';

class WeatherCard extends StatelessWidget {
  final WeatherModel weather;
  const WeatherCard({super.key, required this.weather});

  @override
  Widget build(BuildContext context) {
    // final theme = context.theme;
    return GestureDetector(
      onTap: () => Get.toNamed(Routes.weather,
          arguments: '${weather.location.lat},${weather.location.lon}'),
      child: Stack(
        children: [
          Image.asset(
            Constants.bgCard,
            fit: BoxFit.fill,
            width: 1.sw,
          ),
          Container(
            // padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                10.horizontalSpace,
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      20.verticalSpace,
                      Text(
                        '${weather.current.tempC.round()}${Strings.celsius.tr}',
                        style: context.theme.textTheme.displayLarge?.copyWith(
                          color: Colors.white,
                        ),
                      ),
                      5.verticalSpace,
                      Text(
                        '${Strings.wind.tr}: ${weather.current.windKph} ${Strings.kmh.tr}',
                        style: context.theme.textTheme.titleSmall?.copyWith(
                          color: Colors.white,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        '${weather.location.name}, ${weather.location.country}',
                        style: context.theme.textTheme.displayMedium?.copyWith(
                          color: Colors.white,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                Column(
                  // mainAxisAlignment : MainAxisAlignment.st,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomCachedImage(
                      imageUrl: weather.current.condition.icon
                          .toHighRes()
                          .addHttpPrefix(),
                      fit: BoxFit.cover,
                      width: 100.w,
                      height: 120.h,
                      color: Colors.white,
                    ),
                    10.verticalSpace,
                    SizedBox(
                      width: 100.w,
                      child: Center(
                        child: Text(
                          weather.current.condition.text,
                          style: context.theme.textTheme.displaySmall
                              ?.copyWith(color: Colors.white, fontSize: 12),
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
