import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:searchfield/searchfield.dart';
import 'package:get/get.dart';
import 'package:weather_app/app/routes/app_pages.dart';
import 'package:weather_app/utils/extensions.dart';

import '../../../config/translations/strings_enum.dart';
import '../../../utils/constants.dart';
import '../../widgets/api_error_widget.dart';
import '../../widgets/custom_icon_button.dart';
import '../../widgets/base_body_widget.dart';
import '../../controllers/home_controller.dart';
import 'widgets/home_shimmer_widget.dart';
import 'widgets/weather_card_widget.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 26.w),
          child: GetBuilder<HomeController>(
            builder: (_) => BaseBodyWidget(
              apiCallStatus: controller.apiCallStatus,
              loadingWidget: () => const HomeShimmer(),
              errorWidget: () => ApiErrorWidget(
                retryAction: () => controller.getUserLocation(),
              ),
              successWidget: () => ListView(
                children: [
                  20.verticalSpace,
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              Strings.helloUser.tr,
                              style: context.theme.textTheme.displayMedium,
                            ),
                            8.verticalSpace,
                            Text(
                              Strings.discoverTheWeather.tr,
                              style: context.theme.textTheme.displayMedium,
                            ),
                          ],
                        ),
                      ),
                      20.horizontalSpace,
                      CustomIconButton(
                        onPressed: () => controller.onChangeThemePressed(),
                        icon: GetBuilder<HomeController>(
                          id: controller.themeId,
                          builder: (_) => Icon(
                            controller.isLightTheme
                                ? Icons.dark_mode_outlined
                                : Icons.light_mode_outlined,
                            color: context.theme.iconTheme.color,
                          ),
                        ),
                        borderColor: context.theme.dividerColor,
                      ),
                      8.horizontalSpace,
                      CustomIconButton(
                        onPressed: () => _showCustomBottomSheet(
                          context,
                          controller: controller,
                          title: Strings.changeLanguage.tr,
                          child: Center(
                            child: SizedBox(
                              height: 216.h,
                              child: CupertinoPicker(
                                  scrollController: FixedExtentScrollController(
                                      initialItem: Constants.supportedLocales
                                          .indexWhere((element) =>
                                              element.languageCode ==
                                              controller.currentLanguage)),
                                  itemExtent: 60.w,
                                  onSelectedItemChanged: (index) {
                                    controller.getLanguage = Constants
                                        .supportedLocales[index].languageCode;
                                  },
                                  children: Constants.supportedLocales
                                      .map((e) => _languageItem(e))
                                      .toList()),
                            ),
                          ),
                        ),
                        icon: SvgPicture.asset(
                          Constants.language,
                          fit: BoxFit.none,
                          colorFilter: ColorFilter.mode(
                              context.theme.iconTheme.color!, BlendMode.srcIn),
                        ),
                        borderColor: context.theme.dividerColor,
                      ),
                    ],
                  ),
                  24.verticalSpace,
                  SearchField(
                    controller: controller.searchController,
                    onSearchTextChanged: (text) {
                      if (text.isNotEmpty) {
                        controller.search(text);
                      }
                      return null;
                    },
                    searchInputDecoration: InputDecoration(
                      hintText: Strings.searchCity.tr,
                      prefixIcon: const Icon(Icons.search),
                      suffixIcon: InkWell(
                        onTap: controller.clearText,
                        child: const Icon(Icons.clear),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.r)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.r),
                        ),
                        borderSide: BorderSide(
                          color: Colors.grey,
                          width: 1.w,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.r),
                        ),
                        borderSide: BorderSide(
                          color: Colors.blue,
                          width: 1.w,
                        ),
                      ),
                    ),
                    // hint: "search City",
                    suggestions: controller.locationSearch
                        .map((e) => SearchFieldListItem<String>(e.name,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 4.w, horizontal: 10.w),
                              child: SizedBox(
                                height: 100.w,
                                child: InkWell(
                                  onTap: () => Get.toNamed(Routes.weather,
                                      arguments: '${e.lat},${e.lon}'),
                                  child: Text(
                                    e.name,
                                    style: context.theme.textTheme.displaySmall,
                                  ),
                                ),
                              ),
                            )))
                        .toList(),
                  ),
                  24.verticalSpace,
                  WeatherCard(weather: controller.currentWeather),
                  16.verticalSpace,
                  24.verticalSpace,
                  Text(
                    Strings.aroundTheWorld.tr,
                    style: context.theme.textTheme.displayMedium,
                  ),
                  16.verticalSpace,
                  ListView.separated(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: controller.weatherArroundTheWorld.length,
                    separatorBuilder: (context, index) => 16.verticalSpace,
                    itemBuilder: (context, index) => WeatherCard(
                      weather: controller.weatherArroundTheWorld[index],
                    ),
                  ),
                  24.verticalSpace,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget _languageItem(Locale locale) {
  return Container(
      height: 50.w,
      alignment: Alignment.center,
      child: Text(
        getLocaleFullName(locale),
      ));
}

void _showCustomBottomSheet(BuildContext context,
    {required Widget child,
    required String title,
    required HomeController controller}) {
  showModalBottomSheet(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20.r),
        topRight: Radius.circular(20.r),
      ),
    ),
    context: context,
    builder: (BuildContext context) {
      return Container(
        padding: EdgeInsets.all(16.w),
        child: CupertinoPageScaffold(
          backgroundColor: !context.isDarkMode ? const Color(0xFFF7F6FC) : const Color(0xFF40464a),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    onPressed: () => Get.back(),
                    child: Text(
                      Strings.cancelChangeLanguage.tr,
                    ),
                  ),
                  Text(
                    title,
                  ),
                  TextButton(
                    onPressed: () {
                      controller
                          .onChangeLanguagePressed(controller.getLanguage);
                      Get.back();
                    },
                    child: Text(
                      Strings.confirmChangeLanguage.tr,
                    ),
                  ),
                ],
              ),
              child,
              SizedBox(height: 20.h)
            ],
          ),
        ),
      );
    },
  );
}
