import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../widgets/custom_loading_overlay.dart';
import '../../utils/extensions.dart';
import '../../config/translations/localization_service.dart';
import '../../utils/constants.dart';
import '../data/models/weather_details_model.dart';
import '../services/api_call_status.dart';
import '../services/base_client.dart';

class WeatherController extends GetxController {
  static WeatherController get instance => Get.find();

  final currentLanguage = LocalizationService.getCurrentLocal().languageCode;

  late WeatherDetailsModel weatherDetails;
  late Forecastday forecastday;

  final dotIndicatorsId = 'DotIndicators';
  
  final days = 3;
  var selectedDay = '';

  ApiCallStatus apiCallStatus = ApiCallStatus.holding;

  late PageController pageController;
  
  var currentPage = 0;

  @override
  void onInit() async {
    pageController = PageController(
      initialPage: currentPage, viewportFraction: 0.8,
    );
    super.onInit();
  }
  
  @override
  void onReady() {
    getWeatherDetails();
    super.onReady();
  }

  getWeatherDetails() async {
    await showLoadingOverLay(
      asyncFunction: () async => await BaseClient.safeApiCall(
        Constants.forecastWeatherApiUrl,
        RequestType.get,
        queryParameters: {
          Constants.key: Constants.apiKey,
          Constants.q: Get.arguments,
          Constants.days: days,
          Constants.lang: currentLanguage,
        },
        onSuccess: (response) {
          weatherDetails = WeatherDetailsModel.fromJson(response.data);
          forecastday = weatherDetails.forecast.forecastday[0];
          apiCallStatus = ApiCallStatus.success; 
          selectedDay = weatherDetails.forecast.forecastday.first.date.convertToDay();
          update();
        },
        onError: (error) {
          BaseClient.handleApiError(error);
          apiCallStatus = ApiCallStatus.error;
          update();
        },
      ),
    );
  }

  onDaySelected(String day) {
    selectedDay = day;
    var index = weatherDetails.forecast.forecastday.indexWhere((fd) {
      return fd.date.convertToDay() == day;
    });

    pageController.animateToPage(
      index,
      duration: const Duration(milliseconds: 400),
      curve: Curves.easeIn,
    );
    onCardSlided(index);
  }

  onCardSlided(int index) {
    forecastday = weatherDetails.forecast.forecastday[index];
    selectedDay = forecastday.date.convertToDay();
    currentPage = index;
    update();
  }
}
