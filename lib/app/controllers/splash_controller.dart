import 'package:get/get.dart';

import '../data/local/shared_pref.dart';

class SplashController extends GetxController {

  @override
  void onInit() async {
    String route = SharedPref.innitScreen();
    await Future.delayed(const Duration(seconds: 3));
    Get.offNamed(route);
    super.onInit();
  }

}
