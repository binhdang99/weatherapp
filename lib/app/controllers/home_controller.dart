import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../config/theme/theme.dart';
import '../../config/translations/localization_service.dart';
import '../../utils/constants.dart';
import '../data/local/shared_pref.dart';
import '../data/models/search_model.dart';
import '../data/models/weather_model.dart';
import '../services/api_call_status.dart';
import '../services/base_client.dart';
import '../services/location_service.dart';
import '../views/home_view/widgets/location_dialog_widget.dart';

class HomeController extends GetxController {
  static HomeController get instance => Get.find();

  String currentLanguage = LocalizationService.getCurrentLocal().languageCode;
  late String getLanguage;
  Timer? _debounce;

  late WeatherModel currentWeather;
  List<Search> locationSearch = [];

  List<WeatherModel> weatherArroundTheWorld = [];

  // final dotIndicatorsId = 'DotIndicators';
  final themeId = 'Theme';

  ApiCallStatus apiCallStatus = ApiCallStatus.loading;

  var isLightTheme = SharedPref.getThemeIsLight();

  var activeIndex = 1;

  TextEditingController searchController = TextEditingController();

  @override
  void onInit() async {
    if (!await LocationService().hasLocationPermission()) {
      Get.dialog(const LocationDialog());
    } else {
      getUserLocation();
      getLanguage = currentLanguage;
    }
    super.onInit();
  }

  @override
  onClose() {
    super.onClose();
    _debounce?.cancel();
    searchController.dispose();
  }

  void clearText() {
      searchController.clear();
  }

  getUserLocation() async {
    var locationData = await LocationService().getUserLocation();
    if (locationData != null) {
      await getCurrentWeather(
          '${locationData.latitude},${locationData.longitude}');
    }
  }

  getCurrentWeather(String location) async {
    await BaseClient.safeApiCall(
      Constants.currentWeatherApiUrl,
      RequestType.get,
      queryParameters: {
        Constants.key: Constants.apiKey,
        Constants.q: location,
        Constants.lang: currentLanguage,
      },
      onSuccess: (response) async {
        currentWeather = WeatherModel.fromJson(response.data);
        await getWeatherArroundTheWorld();
        apiCallStatus = ApiCallStatus.success;
        update();
      },
      onError: (error) {
        BaseClient.handleApiError(error);
        apiCallStatus = ApiCallStatus.error;
        update();
      },
    );
  }

  search(String location) async {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () async {
      await BaseClient.safeApiCall(
        Constants.searchApiUrl,
        RequestType.get,
        queryParameters: {
          Constants.key: Constants.apiKey,
          Constants.q: location,
          Constants.lang: currentLanguage,
        },
        onSuccess: (response) async {
          locationSearch.clear();
          locationSearch.addAll(List<Search>.from(
              response.data.map((item) => Search.fromJson(item))));
          // await getWeatherArroundTheWorld();
          apiCallStatus = ApiCallStatus.success;
          update();
        },
        onError: (error) {
          BaseClient.handleApiError(error);
          apiCallStatus = ApiCallStatus.error;
          update();
        },
      );
    });
  }

  getWeatherArroundTheWorld() async {
    weatherArroundTheWorld.clear();
    final cities = ['London', 'Cairo', 'Alaska'];
    await Future.forEach(cities, (city) {
      BaseClient.safeApiCall(
        Constants.currentWeatherApiUrl,
        RequestType.get,
        queryParameters: {
          Constants.key: Constants.apiKey,
          Constants.q: city,
          Constants.lang: currentLanguage,
        },
        onSuccess: (response) {
          weatherArroundTheWorld.add(WeatherModel.fromJson(response.data));
          update();
        },
        onError: (error) => BaseClient.handleApiError(error),
      );
    });
  }

  // onCardSlided(index, reason) {
  //   activeIndex = index;
  //   update([dotIndicatorsId]);
  // }

  onChangeThemePressed() {
    ThemeApp.changeTheme();
    isLightTheme = SharedPref.getThemeIsLight();
    update([themeId]);
  }

  onChangeLanguagePressed(String value) async {
    bool checkNullValue = value.isNotEmpty ;
    getLanguage = checkNullValue ? value : LocalizationService.getCurrentLocal().languageCode;
    await LocalizationService.updateLanguage(getLanguage);
    apiCallStatus = ApiCallStatus.loading;
    currentLanguage = LocalizationService.getCurrentLocal().languageCode;
    update();
    await getUserLocation();
  }
}
