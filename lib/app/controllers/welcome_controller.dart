import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../routes/app_pages.dart';

class WelcomeController extends GetxController {
  routeHome() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool('goHome', true);
    Get.offNamed(Routes.home);
  }
}