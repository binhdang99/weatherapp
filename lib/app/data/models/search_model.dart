class Search {
  num id;
  String name;
  String region;
  String country;
  double lat;
  double lon;

  Search({
    required this.id,
    required this.name,
    required this.region,
    required this.country,
    required this.lat,
    required this.lon,
  });

  factory Search.fromJson(Map<String, dynamic> json) => Search(
        id: json['id'],
        name: json['name'],
        region: json['region'],
        country: json['country'],
        lat: json['lat']?.toDouble(),
        lon: json['lon']?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'region': region,
        'country': country,
        'lat': lat,
        'lon': lon,
      };
}
