import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../config/translations/localization_service.dart';
import '../config/translations/strings_enum.dart';

extension StringExtension on String {
  String toHighRes() {
    return replaceFirst('64x64', '128x128');
  }

  String addHttpPrefix() {
    return 'https:$this';
  }

  String convertToTime() {
    var dateTime = DateTime.parse(this);
    if (dateTime.hour == DateTime.now().hour) return Strings.now.tr;
    return DateFormat('HH:mm').format(dateTime);
  }

  String formatTime() {
    var dateTime = DateFormat("hh:mm a").parse(this);
    return DateFormat("hh:mm").format(dateTime);
  }
}

extension DateTimeExtension on DateTime {
  String convertToDay() {
    if (day == DateTime.now().day) return Strings.today.tr;
    return DateFormat.EEEE(LocalizationService.getCurrentLocal().languageCode)
        .format(this);
  }
}

String getLocaleFullName(Locale locale) {
  var localeNames = {
    'en': 'English',
    'vi': 'Vietnamese',
  };
  return localeNames[locale.languageCode] ?? '';
}
