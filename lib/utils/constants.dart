import 'package:flutter/material.dart';

class Constants {
  static const apiKey = 'a22a139582874495bd642904232010';
  static const baseUrl = 'http://api.weatherapi.com/v1';
  static const currentWeatherApiUrl = '$baseUrl/current.json';
  static const forecastWeatherApiUrl = '$baseUrl/forecast.json';
  static const searchApiUrl = '$baseUrl/search.json';
  static const key = 'key';
  static const q = 'q';
  static const days = 'days';
  static const lang = 'lang';
  static const logo = 'assets/images/app_icon.png';
  static const bgCard = 'assets/images/bg_card.png';
  static const noData = 'assets/images/no_data.png';
  static const search = 'assets/vectors/search.svg';
  static const language = 'assets/vectors/language.svg';
  static const category = 'assets/vectors/category.svg';
  static const downArrow = 'assets/vectors/down_arrow.svg';
  static const wind = 'assets/vectors/wind.svg';
  static const pressure = 'assets/vectors/pressure.svg';
  static const weatherAnimation = 'assets/images/plash_page.gif';
  static const List<Locale> supportedLocales = [
    Locale('en'),
    Locale('vi'),
  ];
}
